# Tarea 2 - Código Enigma
# Curso: Elementos de la Computación
Encriptar es una manera de codificar la información para protegerla frenrte a terceros.
 
 
La encriptación informática es la codificación de la información de archivos o de un correo electrónico para que no pueda ser descifrado en caso de ser interceptado por alguien mientras esta información viaja por la red.

## Requerimientos:
- Debe crearseprograma y subprogramas en python3 con los cuales se pueda encriptar un archivo con un diccionario Entrada Archivo diccionario y archivo a encriptar
- El programa debe iniciarse con las variables configuradas y debe crear un tercer archivo de salida encriptado
- Debe crearse un programa y subprogramas el cual pueda desencriptar con un diccionario y un archivo encriptado como entrada
- El programa debe iniciarse con las variables configuradas y debe crear un tercer archivo de salida dencriptado

## Documentación:
- Documentación interna: Entrada, Salida, Que hace (por función)
- Pseudocódigo
- Diagrama de flujo

 ***Porcentaje:*** 5 % 

***Fecha de Entrega:*** Martes 3 de Septiembre de 2019 antes de las 23:59:59 GMT-6 

***Fecha de Revisión:*** Martes 10 de Septiembre de 2019

***Lenguaje:*** Python3 

***Recurso Humano:*** Grupos de 2 personas máximo

***Metodo de entrega:*** gitlab.com agregar al proyecto a el usuario raraya23 
