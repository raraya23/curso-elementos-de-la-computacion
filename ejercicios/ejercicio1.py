#Entrada: Ninguna
#Salida: String con "Hola Mundo!"
#Que Hace: Esta funcion holamundo() retorna "Hola mundo"
def holamundo():
	holamundostr = "Hola mundo!"
	return holamundostr
##################################################################
#Entrada: Un string
#Salida: impresion del string de entrada
#Que Hace: Esta funcion holamundo() imprime el string de entrada en la terminal
def holamundo2(stringAimprimir):
	return stringAimprimir
########################################################
#Entradas: 2 numeros enteros
#Salida: Un entero 
#Que hace: sumar las 2 entradas y retornar el resultado
def sumar2enteros(entero1, entero2):
	resultado = entero1 + entero2 
	return resultado
##### Gaus
#Entreda: Entrada numero N el cual es el numero maximo en la sumatoria
#Salida: suma de los numeros consecutivos hasta el numero maximo ingresado
#Que hace: suma cada uno de los numeros consecutivos hasta el numero maximo ingresado
def gauss(numeroMaximo):
	resultado = ((numeroMaximo + 1  )* numeroMaximo)/2
	return resultado

##Main
#print (holamundo())
#print (holamundo2("Hola mundo2!"))
print (gauss(100))
