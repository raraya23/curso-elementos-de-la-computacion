# I. Organización de un sistema computacional 
## 1. Antecedentes históricos   

---

### El Abaco

- Posiblemente fue el primer dispositivo mecánico de contabilidad que existió.

- Se ha calculado que tuvo su origen hace al menos 5000 años y su efectividad ha soportado la prueba del tiempo.

![](./imagenes/abaco.jpg)

---


### La Pascalina

El inventor y pintor Leonardo Da Vinci (1452-1519) trazó las ideas para una sumadora mecánica. Siglo y medio después, el filósofo y matemático francés Blas Pascal (1623-1662) inventó y construyó la primera sumadora mecánica.

![](./imagenes/pascalina.jpg)

---

### La computadora

La primera máquina de calcular mecánica, un precursor del ordenador digital, fue inventada en 1642 por el matemático francés Blaise Pascal. Aquel dispositivo utilizaba una serie de ruedas de diez dientes en Las que cada uno de los dientes representaba un dígito del 0 al 9. 

---


### Máquina analítica

![](./imagenes/maquina-analitica.jpeg)

En el siglo XIX el matemático e inventor británico Charles Babbage elaboró los principios de la computadora digital moderna. Inventó una serie de máquinas, como la máquina diferencial, diseñadas para solucionar problemas matemáticos complejos.


---


### Ordenadores Analógicos

A principios del siglo XX, los primeros modelos realizaban los cálculos mediante ejes y engranajes giratorios. Con estas máquinas se evaluaban las aproximaciones numéricas de ecuaciones demasiado difíciles como para poder ser resueltas mediante otros métodos. Durante las dos guerras mundiales se utilizaron sistemas informáticos analógicos, primero mecánicos y más tarde eléctricos, para predecir la trayectoria de los torpedos en los submarinos y para el manejo a distancia de las bombas en la aviación.

![](./imagenes/ordenador-analogico.jpg)

---

### Ordenadores electrónicos


Durante la II Guerra Mundial (1939-1945), un equipo de científicos y matemáticos que trabajaban en Bletchley Park, al norte de Londres, crearon lo que se consideró el primer ordenador digital totalmente electrónico: el Colossus. Hacia diciembre de 1943 el Colossus, que incorporaba 1.500 válvulas o tubos de vacío, era ya operativo. Fue utilizado por el equipo dirigido por Alan Turing para descodificar los mensajes de radio cifrados de los alemanes. 


---
### Primer computador 

En 1939 y con independencia de este proyecto, John Atanasoff y Clifford Berry ya habían construido un prototipo de máquina electrónica en el Iowa State College (EEUU) Este prototipo y las investigaciones posteriores se realizaron en el anonimato, y más tarde quedaron eclipsadas por el desarrollo del Calculador e integrador numérico digital electrónico (ENIAC) en 1945.

![](./imagenes/ENIAC.jpeg)

---
### Primera Generación
#### ENIAC 

El ENIAC contenía 18.000 válvulas de vacío y tenía una velocidad de varios cientos de multiplicaciones por minuto, pero su programa estaba conectado al procesador y debía ser modificado manualmente.
Se construyó un sucesor del ENIAC con un almacenamiento de programa que estaba basado en los conceptos del matemático húngaro-estadounidense John von Neumann. Las instrucciones se almacenaban dentro de una llamada memoria.

![](./imagenes/ENIAC.jpeg)

---
### Segunda Generación
#### Transistor

A finales de la década de 1960 apareció el circuito integrado (CI), que posibilitó la fabricación de varios transistores en un único sustrato de silicio en el que los cables de interconexión iban soldados. El circuito integrado permitió una posterior reducción del precio, el tamaño y los porcentajes de error. El microprocesador se convirtió en una realidad a mediados de la década de 1970

![](./imagenes/transistor.webp)

---
### Segundda Generación
#### Circuito integrado (chips)

Aumenta la capacidad de almacenamiento y se reduce el tiempo de respuesta.
Generalización de lenguajes de programación de alto nivel. Compatibilidad para compartir software entre diversos equipos.

---

### Cuarta Generación
#### Microcircuito integrado
El microprocesador: el proceso de reducción del tamaño de los componentes llega a operar a escalas microscópicas. La microminiaturización permite construir el microprocesador, circuito integrado que rige las funciones fundamentales del ordenador.

![](/home/randall/Documentos/TEC/cursos/curso-elementos-de-la-computacion/presentaciones/imagenes/microcircuitointegrado.jpg)

---
### Quinta Generación 
#### La Inteligencia Artificial

El propósito de la Inteligencia Artificial es equipar a las Computadoras con "Inteligencia Humana" y con la capacidad de razonar para encontrar soluciones. Otro factor fundamental del diseño, la capacidad de la Computadora para reconocer patrones y secuencias de procesamiento que haya encontrado previamente, (programación Heurística)

---
## Personajes importantes
 - Atanasoff Y Berry
 Atanasoff como el inventor de la computadora digital electrónica en 1937 a 1942 (patente)

 - Pascal
 Fue el primero en diseñar y construir una máquina sumadora. Quería ayudar a su padre, quien era cobrador de impuestos
 
---
## Personajes importantes
 - Charles Babagge & Ada Lovelace
 El Babbage del que todo mundo ha leído es, sin embargo, el inventor fracasado que se pasó toda su vida intentando construir la primera computadora de uso general de la historia y que, pese a haber fracasado
 
![](/home/randall/Documentos/TEC/cursos/curso-elementos-de-la-computacion/presentaciones/imagenes/charles-ada.jpeg)

---
## Personajes importantes
 - Gottfried Wihelm Leibniz
 Demostró las ventajas de utilizar el sistema binario en lugar del decimal en las computadoras mecánicas. Inventó y construyó una máquina aritmética que realizaba las cuatro operaciones básicas y calculaba raíces cuadradas.

---
## Personajes importantes
 - John Von Neumann
 Con el advenimiento de la Segunda Guerra Mundial, von Neumann hubo de abandonar sus estudios en matemáticas puras, y concentrarse en problemas más prácticos para servir al Gobierno del que ahora era nacional. Fue consultor en proyectos de balística, en ondas de detonación, y eventualmente, se involucró en el desarrollo de la bomba atómica, en donde demostró la factibilidad de la técnica de implosión que más tarde se usaría en la bomba que detonó en Nagasaki. 

---
## Personajes importantes
 - Herman Hollerith
  Propuso desarrollar un método más práctico para manejar estos datos. En 1889 termino su "máquina tabuladora eléctrica" que lograba registrar datos en tarjetas perforadas. Gracias a este invento se lograban tabular de 50 a 75 tarjetas por minuto y conteos que manualmente se hubieran terminado en años, podían lograrse en pocos meses. Fundó en 1896 la Compañía de Máquinas Tabuladoras para promover el uso comercial de su invento. Más tarde la compañía cambió al nombre de International Business Machine (IBM).

---
## Personajes importantes
 - Howard H. Aiken
Construyó una computadora electromecánica programable siguiendo las ideas introducidas por Babbage
 
- Konrad Zuse
Introdujo interruptores magnéticos, llamados relevadores eléctricos en las computadoras.
 
---
## Personajes importantes
 - Alan Mathison Turing
Diseñó la primera computadora electrónica digital de bulbos. en 1938, Turing tuvo la oportunidad de poner sus teorías en práctica. Bajo su dirección se construyó "Colossus", una máquina cuyo propósito era descifrar el código secreto militar alemán y que fue terminada en 1943

---
## Personajes importantes
- J. Presper Eckert y John W. Mauchly
 Construyeron la computadora electrónica más grande del mundo y utilizaron para ello 18,000 bulbos (ENIAC).

---
## Referencias 
- https://es.wikipedia.org/wiki/Anexo:Historia_de_la_computaci%C3%B3n
- https://www.gestiopolis.com/historia-de-la-computacion-y-la-informatica/
- https://www.monografias.com/trabajos14/antecedentescompu/antecedentescompu.shtml
