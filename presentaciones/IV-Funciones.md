# IV. Funciones

---
## 1. Definición de función 
En computación, una subrutina o subprograma (también llamada procedimiento, función o rutina), como idea general, se presenta como un subalgoritmo que forma parte del algoritmo principal, el cual permite resolver una tarea específica.


---
## 2. Paso de parámetros
- **Paso por valor:** Se crea una copia local de la variable dentro de la función.

- **Paso por referencia:** Se maneja directamente la variable, los cambios realizados dentro de la función le afectarán también fuera.

**Como ya sabemos los números se pasan por valor y crean una copia dentro de la función, por eso no les afecta externamente**


---
## 2. Paso de parámetros
```python
##################################################################
#Entrada: Un string
#Salida: impresion del string de entrada
#Que Hace: Esta funcion holamundo() imprime el string de entrada en la terminal

def holamundo2(stringAimprimir):
	return stringAimprimir
########################################################
#Entradas: 2 numeros enteros
#Salida: Un entero 
#Que hace: sumar las 2 entradas y retornar el resultado
def sumar2enteros(entero1, entero2):
	resultado = entero1 + entero2 
	return resultado

```

---
## 3. Valores por omisión en los argumentos

```python
def funcion(entero1, entero2):
	print(entero1 + entero2)
```
```python
funcion(1, 2)
>>>3

```
```python
def funcion(entero1=0, entero2=0):
	print(entero1, entero2)
```
```python
funcion()
>>>0
```

---
## Referencias
- http://elclubdelautodidacta.es/wp/2011/12/python-capitulo-27-funciones-con-argumentos-por-defecto/
- https://docs.hektorprofe.net/python/programacion-de-funciones/paso-por-valor-y-referencia/