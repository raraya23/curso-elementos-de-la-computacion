# V. Herramientas de control de flujo

---
## 1. Construcciones de selección (if, else, elife, case)
```python

if una_variable > 10:
    print("una_variable es completamente mas grande que 10.")
elif una_variable < 10:    # Este condición 'elif' es opcional.
    print("una_variable es mas chica que 10.")
else:           # Esto también es opcional.
    print("una_variable es de hecho 10.")
```

---
## 1. Construcciones de selección (if, else, elife, case)
```python
def switch_demo(argument):
    switcher = {
        1: "January",
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December"
    }
    print switcher.get(argument, "Invalid month")
```

---
## 2. Construcciones de Iteración (for, while)

```python
x = 0
while x < 4:
    print(x)
    x += 1  # versión corta de x = x + 1

```


---
## 2. Construcciones de Iteración (for, while)

```python
for i in range(4):
    print(i)
```


---
## Referencias
- https://gitlab.com/raraya23/curso-elementos-de-la-computacion/raw/master/presentaciones/IV-Funciones.md