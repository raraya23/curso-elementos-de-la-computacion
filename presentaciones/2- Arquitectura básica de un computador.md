# I. Organización de un sistema computacional 
## 2. Arquitectura básica de un computador: memoria, unidad central de procesamiento, almacenamiento secundario. 

---
### Hardware

- Hardware: Partes tangibles de una computadora

![](./imagenes/hardware.jpg)

---
#### Unidad Central de procesamiento 
Central processing unit (**CPU**) es el hardware dentro de un ordenador u otros dispositivos programables, que interpreta las instrucciones de un programa informático mediante la realización de las operaciones básicas aritméticas, lógicas y de entrada/salida del sistema. 

---
#### Unidad Central de procesamiento 
##### Arquitectura Basica CPU
- CPU son la unidad aritmético lógica (**ALU**), que realiza operaciones aritméticas y lógicas
 
- Unidad de control (**CU**), que extrae instrucciones de la memoria, las decodifica y las ejecuta, llamando a la ALU cuando sea necesario.

---
#### Unidad Central de procesamiento 
##### Tipos de Arquitectura Basica CPU
CISC (**Complex Instruction Set Computer**) o ejecución de una instrucción compleja, que forma la base de todos los procesadores x86 o compatibles con intel, arquitectura

RISC (**Reduced Instruction Set Computer**) o que ejecuta un número limitado de instrucciones

VLIW (**Very Long Instruction Word**)

EPIC (**Explicit Parallel Instruction Computing**)


---
#### Almacenamiento Primario
Es una porción de memoria a la que accede el procesador antes de tener que salir “al mundo externo” y acceder a la RAM.
![](./imagenes/CPUCACHERAM.png)

---
#### Almacenamiento Primario
![](./imagenes/cacheymemoria.png)

---
#### Almacenamiento Primario
Memoria Cache Nivel 1 (**L1**):

Esta memoria cache es extremadamente rápida pero relativamente pequeña y hoy día se encuentra integrada en el CPU (años atrás podía o no estar integrada en el CPU). Todas las instrucciones se buscan primero aquí, si no están presentes entonces se procede al siguiente nivel.

---
#### Almacenamiento Primario
Memoria Cache Nivel 2 (**L2**):

Esta memoria cache es considerablemente más grande que L1 y también está dentro del CPU (años atrás no lo estaba). Si las instrucciones no fueron encontradas en el Nivel L1 entonces se buscan en este Nivel L2, este tipo de memoria no es tan rápida como la usada en L1 por tanto es de esperar un poco de latencia (demora).

---
#### Almacenamiento Primario
 Memoria Cache Nivel 3 (**L3**): Este es un nivel de memoria especializada que ayuda a mejorar el rendimiento de los Niveles de Cache L1 y L2. Es mucho más lenta que la memoria L1 o L2, pero mucho más rápida que la memoria RAM del Sistema. En el caso de los Procesadores con muchos Cores, cada uno de ellos tiene su propio Cache L1 y Cache L2, pero, todos comparten el mismo Cache L3. Cuando una instrucción es buscada en L3 se eleva a un cache de un nivel más alto.
 
---
#### Almacenamiento Secundario
---

##### Memoria
Es el dispositivo que retiene, memoriza o almacena datos informáticos durante algún período de tiempo.

----
##### Memoria
- RAM (**Random Acces Memory**): Memoria volátil.

- ROM (**Read-Only Memory, ROM**): La memoria de sólo lectura, retiene la información almacenada en el momento de fabricarse y 

- WORM (**Write Once Read Many**): la memoria de escritura única lectura múltiple  permite que la información se escriba una sola vez en algún momento tras la fabricación. También están las memorias inmutables, que se utilizan en memorias terciarias y fuera de línea. Un ejemplo son los CD-ROM.

----
##### Memoria
- Las memorias de escritura lenta y lectura rápida son memorias de lectura/escritura que permite que la información se reescriba múltiples veces pero con una velocidad de escritura mucho menor que la de lectura. Un ejemplo son los CD-RW.

- EPROM (**Erasable Programmable Read-Only Memory**): memoria persistente programable

----
##### Discos Duros (**HD**)
 
- Se compone de uno o más platos o discos rígidos, recubiertos con material magnético y unidos por un mismo eje que gira a gran velocidad dentro de una caja metálica sellada. 

- Es un dispositivo de almacenamiento de datos

----
##### Discos Duros (**HD**)
![](./imagenes/discoduro.png)

---

# I. Organización de un sistema computacional
### 3. Componentes de software: Software del sistema, Programas de uso general, Desarrollo de aplicaciones

---
### Software

- Software:Es el conjunto de los programas de cómputo, procedimientos, reglas, documentación y datos asociados, que forman parte de las operaciones de un sistema de computación.(Extraído del estándar 729 del IEEE)

![](./imagenes/Software.jpg)


---
### Software
#### Etapas Mínimas
- Captura, elicitación, especificación y análisis de requisitos (ERS)
- Diseño
- Codificación
- Pruebas (unitarias y de integración)
- Instalación y paso a producción
- Mantenimiento

---
### Software
#### Clasificación del software
Software de sistema: Su objetivo es desvincular adecuadamente al usuario y al programador de los detalles del sistema informático en particular que se use
  - Sistemas operativos
  - Controladores de dispositivos
  - Herramientas de diagnóstico
  - Herramientas de corrección y optimización
  - Servidores
  - Utilidades

---
### Software
#### Software de programación:
 - Editores de texto
 - Compiladores
 - Intérpretes
 - Enlazadores
 - Depuradores

---
### Software
#### Software de aplicación:
 - Aplicaciones para Control de sistemas y automatización industrial
 - Aplicaciones ofimáticas
 - Software educativo
 - Software empresarial
 - Bases de datos
 - Telecomunicaciones (por ejemplo Internet y toda su estructura lógica)
 - Videojuegos
 - Software médico
 - Software de cálculo numérico y simbólico.
 - Software de diseño asistido (CAD)
 - Software de control numérico (CAM)

---
### Software
#### Lenguajes Interpretados vs. Compilados
---
### Referencias
- https://ed.team/blog/lenguajes-compilados-vs-lenguajes-interpretados
- https://es.wikipedia.org/wiki/Software
- http://www.ieec.uned.es/investigacion/Dipseil/PAC/archivos/Formacion_Especifica_Tarea_ISE3_4_2.pdf 
- https://es.wikipedia.org/wiki/Memoria_EPROM
- https://uruguayoc.com/2018/03/18/que-es-el-cache-l1-l2-y-l3-en-los-procesadores/
- https://es.wikipedia.org/wiki/Memoria_(inform%C3%A1tica)
- https://es.wikipedia.org/wiki/Unidad_de_disco_duro




