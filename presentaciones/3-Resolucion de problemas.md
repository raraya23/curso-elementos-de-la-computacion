# II. Resolución de problemas

---
## 1. Definición del problema 
Consiste en expresar en una forma clara el problema y cuales son los resultados que se espera obtener, es decir evitando la dualidad.

---
## 2. Entradas, salidas, proceso y limitaciones 
### Entradas 
Las entradas son los ingresos del sistema que pueden ser recursos materiales, recursos humanos o información.

Las entradas constituyen la fuerza de arranque que suministra al sistema sus necesidades operativas.

- **En serie:** es el resultado o la salida de un sistema anterior con el cual el sistema en estudio está relacionado en forma directa.
- **Aleatoria:** es decir, al azar, donde el termino "azar" se utiliza en el sentido estadístico. Las entradas aleatorias representan entradas potenciales para un sistema.
- **Retroacción:** es la reintroducción de una parte de las salidas del sistema en sí mismo.

---
## 2. Entradas, salidas, proceso y limitaciones 
### Salidas
Las salidas de los sistemas son los resultados que se obtienen de procesar las entradas. Al igual que las entradas estas pueden adoptar la forma de productos, servicios e información. Las mismas son el resultado del funcionamiento del sistema o, alternativamente, el propósito para el cual existe el sistema.

Las salidas de un sistema se convierten en entrada de otro, que la procesará para convertirla en otra salida, repitiéndose este ciclo indefinidamente.

---
## 2. Entradas, salidas, proceso y limitaciones 
### Proceso  
El proceso es lo que transforma una entrada en salida, como tal puede ser una máquina, un individuo, una computadora, un producto químico, una tarea realizada por un miembro de la organización, etc.

---
## 2. Entradas, salidas, proceso y limitaciones 
### Limitaciones 
Cada sistema tiene algo interior y algo exterior, asi mismo lo que es externo al sistema, forma parte del ambiente y no al propio sistema.

Los límites se encuentran intimamente vinculados con la cuestión del ambiente, lo podemos definir como la línea que forma un circulo alrededor de variables seleccionadas tal que existe un menor intercambio de energía atravez de esa línea con el interior del circulo que delimita.

---
## 3. Algoritmos (diagrama flujo - pseudocódigo) 
### Algoritmos
Grupo finito de operaciones organizadas de manera lógica y ordenada que permite solucionar un determinado problema. Se trata de una serie de instrucciones o reglas establecidas que, por medio de una sucesión de pasos, permiten arribar a un resultado o solución.

---
## 3. Algoritmos (diagrama flujo - pseudocódigo) 
### Diagrama flujo - pseudocódigo
![](./imagenes/pseudocodigo-algoritmo.gif)



---
## Referencias
- https://www.monografias.com/trabajos5/teorsist/teorsist.shtml#entra