# III. Elementos básicos del lenguaje de programación (Python3)

---
## Expresiones y sentencias 

Una sentencia es una instrucción que el intérprete de Python puede ejecutar.
Ejemplo:
Comando
```python
print(1)
```
Salida
```python
>>> 1
```

---
## tipos de dato 
### numericas
- Entero 0,1,2,3......(int)
- (Real) Float 0.0,0.1.....(float)

---
## Tipos de datos 
### No numericas
- caracteres (chars Strings) "Hola Mundo!" (str)
- Booleanos (bool)
---
## Variables
Una de las características más poderosas en un lenguaje de programación es la capacidad de manipular variables. Una variable es un nombre que se refiere a un valor la cual tiene un tipo y valor asociado.
![](./imagenes/palabras-reservadas-python.png)

---
## Operadores 
Los operadores son símbolos especiales que representan cómputos como la suma y la multiplicación. Los valores que el operador usa se denominan operandos.

Los símbolos +, - y /, y los paréntesis para agrupar, significan en Python lo mismo que en matemáticas. El asterisco (*) es el símbolo para la multiplicación, y ** es el símbolo para la potenciación.

Cuando el nombre de una variable aparece en la posición de un operando, se reemplaza por su valor antes de realizar la operación.

```python

>>> minuto = 59
>>> minuto/60
0
```
---
## Comentarios 
```python
# linea de comentario 
'''
comentario multilinea

'''
```
---
## Indentación 

![](./imagenes/identacion.jpg)


---
## Referencias
- http://www.openbookproject.net/thinkcs/archive/python/spanish2e/cap02.html
- https://es.slideshare.net/MarioGarciaValdez/python-para-principiantes